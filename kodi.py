#pip install wakeonlan
import sys
import requests
user_args = sys.argv[1:]
url = "http://192.168.178.35/jsonrpc"
headers = {
    'Content-Type': "application/json",
    'cache-control': "no-cache"
    }
from wakeonlan import send_magic_packet

if user_args[0] == 'POWERON':
    send_magic_packet('D0.50.99.00.F4.BF')
if user_args[0] == 'SHUTDOWN':
    payload = "{\"jsonrpc\":\"2.0\",\"method\":\"Addons.ExecuteAddon\",\"params\":{\"addonid\":\"script.recording.autoshutdown\"}}"
    response = requests.request("POST", url, data=payload, headers=headers)
    #print(response.text)
#Shutdown without plugin
#if user_args[0] == 'ADDONSHUTDOWN':
#    payload = "{\"jsonrpc\":\"2.0\",\"method\":\"System.Shutdown\",\"id\":1}"
#    response = requests.request("POST", url, data=payload, headers=headers)
#    #print(response.text)
