# Recording Autoshutdown Addon

This addon allows you to automatically shutdown your KODI instance after any recording. Before shutting down the addon checks if the user is currently activevly looking something. If not it will check if a running recording is active or any is scheduled in a predefined time. The time between the process of shutdown and the next scheduled recording can be set in the options.

## Options

- TVHeadend IP-Address - Set the IP-Address of your pvr-backend
- TVHeadend Port - Set the Port of your pvr-backend
- Waiting-Time - Set the waiting time between shutdown process (user-driven or recording finished) end the ne scheduled recording

## Decision matrix (need to be updated)

If any external/remote kodi is currently playing or a subscription is active localhost will never shut down.

Basic User activity checks

- Check if a active subscription @tvheadend is present
- Check remote kodi for active playing (video, tv, ...)

Basic Recording state

- Check if recording is running - if yes - do nothing - if no - check if next recording time is more then 30 min away - if yes - shutdown - if no - do nothing

## Requirements

- Kodi version: Krypton

## Installation

##### Addon installation

Grab the latest version of the addon and install it via custom zip

##### Settings

- Define your pvr-backend ip-address and specify the related port
- Define the waiting time

##### TVHeadend Configuration

- Got to Configuration > recordings > Choose your active profile > Set postprocessing script to (based on libreelec)
  > /bin/bash /storage/.kodi/addons/script.recording.autoshutdown /startup.sh

## Use in shutdown menu (experimental)

It is possible to use the addon instead of the normal shutdown process to prevent corruption of a currently running recording by adapting the current skin (this changes the behaviour of the power button). Edit the ‘**DialogButtonMenu.xml**’ (or similar) in the xml part of the skin addon and look for a xml tag like (note the **Powerdown( )** inside here):
_Example skin.arctic.zephyr_
_Path: .kodi\addons\skin.arctic.zephyr\1080i_

      <control type="button" id="3116">
           <description>Shutdown</description>
           <include>DefContextButton</include>
           <label>13005</label>
           <onclick>Powerdown()</onclick>
           <visible>System.CanPowerDown</visible>
      </control>

to

      <control type="button" id="3116">
           <description>Shutdown</description>
           <include>DefContextButton</include>
           <label>13005</label>
           <onclick>Powerdown()</onclick>
           <visible>System.CanPowerDown + !System.HasAddon(script.recording.autoshutdown)</visible>
      </control>

      <control type="button" id="3116">
        <description>Shutdown</description>
        <include>DefContextButton</include>
        <label>13005</label>
        <onclick>RunScript(script.recording.autoshutdown)</onclick>
        <visible>System.CanPowerDown + System.HasAddon(script.recording.autoshutdown)</visible>
      </control>

**Important:** Everytime you update the skin you have to redo the same

## Development

##### Active development

Use the addon folder and change python files directly. Start the plugin through user interface via "execute".

> /storage/.kodi/addons/script.timesetter.recordings

##### Log Files / Debugging

To get the debug logging to work, just set the global kodi logging to true and the debug logging in your addons settings. Depending on log level it is probably not needed.

Here is the reason, in case this helps others. The log levels are as follows:

    LOGDEBUG
    LOGINFO
    LOGNOTICE
    LOGWARNING
    LOGERROR
    LOGSEVERE
    LOGFATAL
    LOGNONE

By default, it seems that xbmc.log("message") will log in log level DEBUG. However, unless you activate the debug mode (or tweak advancedsettings.xml), nothing below LOGNOTICE will be recorded in log files.

In other words, if you want an addon to write something in log files, you need to do (at least) xbmc.log("message",level=xbmc.LOGNOTICE)

Use xbmc.log to print something into log (cast to str if needed):

> xbmc.log("message",level=xbmc.LOGNOTICE)

The log file can be found here:

> /storage/.kodi/temp/kodi.log

##### Settings

- Create a keymapping for reloading your skin (any change will need a reload of your skin > \.kodi\userdata\keymaps > <f5>XBMC.ReloadSkin()</f5>
- Changing strings.po - Always return to plugins menu and open options again

##### Zip generation

- Use the included script to generate the zip file
  > python deploy_addon.py <script_id>

## Todo

[x] Update decision matrix (done) - 20200531
[ ] Allow setting of shutdown timer in options
[ ] Add options for external client check (ip, port)
[ ] Add plugin infos

# Welcome to your addon

1. You might want to move this folder into the kodi addon folder for convinience when debugging. It might also be needed to be `enabled` inside of the kodi addon browser.
2. Now start coding! Just open up the `.py` file in this folder and create what you would like Kodi to do! If you're creating a plugin, please check out [this kodi routing framework](https://github.com/tamland/kodi-plugin-routing) and copy a version of that module to your kodi addon folder.
3. Write some tests, maybe? Don't forget to activate [travis](https://travis-ci.org/) access to your repository. We've created a test folder and a travis config file for that, otherwise just delete those ;)
4. You might want to look at your `addon.xml` it should already be filled, but you will need to understand what your doing and might want to fill in some more info. So read up [here](http://kodi.wiki/view/Addon.xml).
5. Do you want some settings for your addon? Check the `settings.xml` in the resources folder. And read up [here](http://kodi.wiki/view/Settings.xml).
6. Read [this info](http://kodi.wiki/view/Add-on_structure#icon.png) and drop an icon for your addon into the `resource` folder and name it `icon.png`.
7. Read [this](http://kodi.wiki/view/Add-on_structure#fanart.jpg) and drop a addon background into the `resource` folder and name it `fanart.jpg`.
8. End up with a beautiful Kodi addon! Good for you :) Maybe you want to [share it with us](http://kodi.wiki/view/Submitting_Add-on_updates_on_Github)?
