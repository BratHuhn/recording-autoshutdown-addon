# -*- coding: utf-8 -*-

from resources.lib import kodiutils
from resources.lib import kodilogging
import logging
import xbmcaddon
import xbmcgui
import xbmc

import requests
import time

ADDON = xbmcaddon.Addon()
logger = logging.getLogger(ADDON.getAddonInfo('id'))
kodiIP = ADDON.getSetting("tvheadendipaddress")
kodiTVHEPort = ADDON.getSetting("tvheadendport")
waitingTime = ADDON.getSetting("tvheadendport")

# Put your code here, this is just an example showing
# a textbox as soon as this addon gets called
def main():
    addon_name = ADDON.getAddonInfo('name')
    recordings = getRecordings()
    currentTime = time.time()
    definedSkipTime = float(waitingTime) # in seconds
    result = ""
    #isPlaying = xbmc.Player().isPlaying()
    isPlaying = getActiveState()
    #xbmc.log(str(isPlaying),level=xbmc.LOGNOTICE)

    if len(recordings["recording"]) > 0:
        toastNotification(addon_name, "Recording running - Shutdown not possible")
        # Do nothing
    else:
        if len(recordings["scheduled"]) == 0:
            if isPlaying == False:
                toastNotification(addon_name, "No recordings active/planned - Shutting down")
                time.sleep(5)
                xbmc.executebuiltin('ShutDown')
            else:
                toastNotification(addon_name, "Someone is watching TV or is streaming - Shutdown not possible")
        else:
            possibleShutdown = 1
            for recording in recordings["scheduled"]:
                # Check if in next "definedSkipTime" recording is starting
                if recording["start_real"] <= (currentTime + definedSkipTime):
                    possibleShutdown = 0
                    toastNotification(addon_name, "Recording schedule in time - Shutdown not possible")
                    break
            if possibleShutdown == 1:
                if isPlaying == False:
                    toastNotification(addon_name, "Recordings schedule exceeding skiptime - Shutting down")
                    time.sleep(5)
                    xbmc.executebuiltin('ShutDown')
                else:
                    toastNotification(addon_name, "Active Player - Shutdown not possible")

def toastNotification(addon_name, message):
    xbmcgui.Dialog().notification(addon_name, message, xbmcgui.NOTIFICATION_INFO, 5000) 

def getActiveState():
    if getActiveSubscriptions() or getActivityOnShield():
        # Shutdown not possible
        return True
    else:
        # Shutdown possible
        return False


def getActivityOnShield():
    url = 'http://192.168.178.67:8080/jsonrpc?request={"jsonrpc":"2.0","method":"Player.GetActivePlayers","id":1}'
    headers = {
        'cache-control': "no-cache",
    }
    response = requests.request("GET", url, headers=headers).json()
    #xbmc.log(str(len(response['result'])),level=xbmc.LOGNOTICE)
    if len(response['result']) > 0:
        # Active external player - no shutdown
        return True
    else:
        # No external player - shutdown legit
        return False

# Return true if currently someone is watching TV, false if nobody looks tv
# Request to tvheadend
def getActiveSubscriptions():
    url = "http://" + kodiIP + ":" + kodiTVHEPort + "/api/status/subscriptions"
    subscriptions = {}
    headers = {
        'cache-control': "no-cache",
    }
    response = requests.request("GET", url, headers=headers).json()
    if len(response["entries"]) > 0:
        # Active subscription - no shutdown possible
        return True
    else:
        # No active subscription - shutdown possible
        return False

def getRecordings():
    url = "http://" + kodiIP + ":" + kodiTVHEPort + "/api/dvr/entry/grid"
    recordings = {}
    recordings["scheduled"] = []
    recordings["recording"] = []
    headers = {
        'cache-control': "no-cache",
    }

    response = requests.request("GET", url, headers=headers).json()
    #print(response.json()["entries"])
    # Loop over recordings to track scheduled recordings
    for recording in response["entries"]:
        #printf(recording)
        if recording["sched_status"] == "scheduled":
            recordings["scheduled"].append(recording)
            #print(recording["stop_real"])
        elif recording["sched_status"] == "recording":
            recordings["recording"].append(recording)
            #print(recording["stop_real"])

    return recordings
